exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable("Feedback", function(table) {
      table.increments("id").primary();
      table.float("rating");
      table.boolean("recommendation");
      table.string("comment");
      table.string("extraColumn1");
      table.boolean("showComment");
      table.boolean("extraColumn2");
      table.integer("senderid");
      table.string("sendername");
      table.string("receivername");
      table.integer("taskid");
      table.integer("receiverid");
      table.dateTime("created_at");
      table.dateTime("updated_at");
      table.unique(["senderid", "taskid", "receiverid"]);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTableIfExists("Feedback")]);
};
