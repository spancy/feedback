var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/feedbackPostHelper");
var getHelper = require("../helpers/feedbackGetHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.feedbackInsert(req);
    res.json(response);
  });

router
  .route("/getByReceiverID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.feedbackGetData(req, "receiverid");
    res.json(response);
  });

router
  .route("/getBySenderID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.feedbackGetData(req, "senderid");
    res.json(response);
  });

router
  .route("/getByTaskID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.feedbackGetData(req, "taskid");
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.feedbackGetData(req, "id");
    res.json(response);
  });

router
  .route("/getByTaskReceiver/:taskid/:receiverid")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.feedbackGetTaskReceiverData(
      req,
      "taskid",
      "receiverid"
    );
    res.json(response);
  });
module.exports = router;
