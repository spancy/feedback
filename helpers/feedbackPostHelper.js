var { QueryBuilder } = require("objection");
var Feedback = require("../models/Feedback");

//function to insert into template table and relations.
async function feedbackInsert(req) {
  const feedbackInsert = {
    rating: req.body.rating,
    comment: req.body.comment,
    senderid: req.body.senderid,
    sendername: req.body.sendername,
    receiverid: req.body.receiverid,
    receivername: req.body.receivername,
    taskid: req.body.taskid,
    recommendation: req.body.recommendation
  };

  try {
    let message = "";
    const feedback = await Feedback.query().insertGraph(feedbackInsert);

    message = {
      statusCode: 200,
      body: {
        message: "New Feedback Created",
        id: feedback.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Feedback Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.feedbackInsert = feedbackInsert;
