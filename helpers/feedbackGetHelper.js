var { QueryBuilder } = require("objection");
var Feedback = require("../models/Feedback");

//function to get all templates and its underlying relations
async function feedbackGetData(req, queryBy) {
  try {
    const feedbackData = await Feedback.query().where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: feedbackData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function feedbackGetTaskReceiverData(req, queryByTask, queryByReceiver) {
  try {
    const feedbackGetTaskReceiverData = await Feedback.query()
      .where(queryByTask, req.params.taskid)
      .where(queryByReceiver, req.params.receiverid);

    let message = "";

    message = {
      statusCode: 200,
      body: feedbackGetTaskReceiverData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function feedbackReceiverData(req, queryByReceiver) {
  try {
    const feedbackGetTaskReceiverData = await Feedback.query()
      .where(queryByReceiver, req.params.receiverid);

    let message = "";

    message = {
      statusCode: 200,
      body: feedbackGetTaskReceiverData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations

module.exports.feedbackGetData = feedbackGetData;
module.exports.feedbackGetTaskReceiverData = feedbackGetTaskReceiverData;
module.exports.feedbackReceiverData = feedbackReceiverData;
